using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class RegistrarCtrl : MonoBehaviour
{
    public InputField nombre_completo;
    public InputField apodo;
    public InputField correo;
    public InputField edad;

    private PerfilesCtrl manager;
    private int siguienteID;

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.Find("Manager").GetComponent<PerfilesCtrl>();

        nombre_completo.text = "Adalberto L�pez Torres";
        apodo.text = "Lotus";
        correo.text = "adalberto.lopez.torres@gmail.com";
        edad.text = "28";

        if (PlayerPrefs.HasKey("siguienteID"))
        {
            siguienteID = PlayerPrefs.GetInt("siguienteID");
        }
        else
        {
            siguienteID = 1;
        }

    }

    public void GuardarPerfil()
    {
        Perfil p = new Perfil(siguienteID, nombre_completo.text, apodo.text, correo.text, edad.text);
        PerfilesCtrl.perfiles.Add(p);
        siguienteID++;
        PlayerPrefs.SetInt("siguienteID", siguienteID);
        PlayerPrefs.SetString("perfiles", JsonConvert.SerializeObject(PerfilesCtrl.perfiles));
        manager.CancelarRegistro();
    }

    public void Cancelar()
    {
        manager.CancelarRegistro();
    }
}
