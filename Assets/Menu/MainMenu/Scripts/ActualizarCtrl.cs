using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
public class ActualizarCtrl : MonoBehaviour
{
    public InputField nombre_completo;
    public InputField apodo;
    public InputField correo;
    public InputField edad;
    public SeleccionadorJugarCtrl seleccionCtrl;

    private Perfil perfilEditado;
    private PerfilesCtrl manager;

    private void Start()
    {
        manager = GameObject.Find("Manager").GetComponent<PerfilesCtrl>();
    }
    public void SetPerfil()
    {
        Perfil p = PerfilesCtrl.perfiles.Find(pr => pr.id == PerfilesCtrl.idPerfilSeleccionado);
        perfilEditado = p;
        nombre_completo.text = perfilEditado.nombre_completo;
        apodo.text = perfilEditado.apodo;
        correo.text = perfilEditado.correo_electronico;
        edad.text = perfilEditado.edad.ToString();
    }

    public void EliminarUsuario()
    {
        Perfil p = PerfilesCtrl.perfiles.Find(pr => pr.id == PerfilesCtrl.idPerfilSeleccionado);
        PerfilesCtrl.perfiles.Remove(p);
        PlayerPrefs.SetString("perfiles", JsonConvert.SerializeObject(PerfilesCtrl.perfiles));
        manager.SeleccionaPerfilParaJugar();
        seleccionCtrl.CerrarVentanaEdicion();
        //CancelarRegistro();

    }

    public void ActualizarPerfil()
    {
        Perfil p = PerfilesCtrl.perfiles.Find(pr => pr.id == PerfilesCtrl.idPerfilSeleccionado);
        p.nombre_completo = nombre_completo.text;
        p.apodo = apodo.text;
        p.correo_electronico = correo.text;
        p.edad = int.Parse(edad.text);
        PlayerPrefs.SetString("perfiles", JsonConvert.SerializeObject(PerfilesCtrl.perfiles));
        manager.SeleccionaPerfilParaJugar();
        seleccionCtrl.CerrarVentanaEdicion();
        //CancelarInterfazPerfil();
    }
}
