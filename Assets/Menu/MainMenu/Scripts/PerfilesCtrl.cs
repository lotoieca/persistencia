using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
public class PerfilesCtrl : MonoBehaviour
{
    public GameObject InterfazRegistro;
    public GameObject ActualizarPerfil;
    public GameObject SeleccionadorPerfil;

    public GameObject ContenedorPerfiles;
    public GameObject ContenedorPerfilesSeleccionar;

    public static List<Perfil> perfiles = new List<Perfil>();
    public static int idPerfilSeleccionado = 0;

    public GameObject prefabPerfilContainer;
    public GameObject prefabPerfilContainerSeleccionar;


    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
    }

    // Start is called before the first frame update
    void Start()
    {
        //perfiles = JsonConvert.DeserializeObject<List<Perfil>>(PlayerPrefs.GetString("perfiles"));
        //Debug.Log(perfiles.Count);
        //Debug.Log(JsonConvert.SerializeObject(perfiles));

        SeleccionadorPerfil.SetActive(false);
        //ActualizarPerfilVentana.SetActive(false);
        CancelarRegistro();

    }

    public void CrearNuevoPerfil()
    {
        InterfazRegistro.SetActive(true);
    }

    public void CancelarRegistro()
    {
        ActualizarListadoPerfiles(ContenedorPerfiles, prefabPerfilContainer);
        InterfazRegistro.SetActive(false);
    }

    public void ActualizarListadoPerfiles(GameObject Contenedor, GameObject prefab)
    {
        if(Contenedor.transform.childCount != 0)
        {
            foreach (Transform child in Contenedor.transform)
            {
                Destroy(child.gameObject);
            }
        }

        if (PlayerPrefs.HasKey("perfiles"))
        {
            perfiles = JsonConvert.DeserializeObject<List<Perfil>>(PlayerPrefs.GetString("perfiles"));
        }

        foreach (Perfil p in perfiles)
        {
            GameObject perfilNuevo = Instantiate(prefab);
            perfilNuevo.name = p.id.ToString();
            perfilNuevo.transform.Find("Text").GetComponent<Text>().text = p.apodo.ToString();
            perfilNuevo.transform.SetParent(Contenedor.transform);
            perfilNuevo.GetComponent<AvatarDataCtrl>().data = p;
            perfilNuevo.GetComponent<AvatarDataCtrl>().EditarPerfilV = ActualizarPerfil;
        }
    }

    public void SalirDelJuego()
    {
        Application.Quit();
    }
   

    public void SeleccionaPerfilParaJugar()
    {
        SeleccionadorPerfil.SetActive(true);
        ActualizarListadoPerfiles(ContenedorPerfilesSeleccionar,prefabPerfilContainerSeleccionar);
        ActualizarListadoPerfiles(ContenedorPerfiles, prefabPerfilContainer);
    }

    public void SalirSeleccionPerfilParaJugar()
    {
        SeleccionadorPerfil.SetActive(false);
    }


}
