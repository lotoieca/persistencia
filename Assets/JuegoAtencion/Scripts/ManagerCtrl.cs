using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;
public class ManagerCtrl : MonoBehaviour
{
    public Image barra;
    public Text puntos;
    public Text RecordTexto;
    public Text GameOver;
    public float tiempoRespuesta = 1.5f;

    private List<Perfil> listadoPerfiles;
    private Perfil jugador;

    private GameObject[] direcciones;
    private GameObject seleccion;
    private float iniciarCuenta;
    private int record;
    private bool sigueEnJuego = false;

    // Start is called before the first frame update
    void Start()
    {
        listadoPerfiles = JsonConvert.DeserializeObject<List<Perfil>>(PlayerPrefs.GetString("perfiles"));
        int idSeleccionado = PlayerPrefs.GetInt("idPerfilSeleccionado");
        jugador = listadoPerfiles.Find(p => p.id == idSeleccionado);
        Debug.Log("Jugador Seleccionado: "+ JsonConvert.SerializeObject(jugador));
        direcciones = GameObject.FindGameObjectsWithTag("direcciones");
        RecordTexto.text = jugador.recordAtencion.ToString();
        Reinicia();
    }

    public void Reinicia()
    {
        GameOver.text = "";
        record = 0;
        sigueEnJuego = true;
        iniciarCuenta = Time.realtimeSinceStartup +  1;
        GetRandomDireccion();
    }

    // Update is called once per frame
    void Update()
    {
        if (sigueEnJuego)
        {
            barra.fillAmount = (iniciarCuenta - Time.realtimeSinceStartup) / tiempoRespuesta;
            puntos.text = "Puntos: " + record;

            if(iniciarCuenta <= Time.realtimeSinceStartup)
            {
                sigueEnJuego = false;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if(seleccion.name == "arriba")
                {
                    Debug.Log("Acierto");
                    record++;
                }
                else
                {
                    Debug.Log("Equivocado");
                    VerificarRecord();
                }

                GetRandomDireccion();
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (seleccion.name == "abajo")
                {
                    Debug.Log("Acierto");
                    record++;
                }
                else
                {
                    Debug.Log("Equivocado");
                    VerificarRecord();
                }

                GetRandomDireccion();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (seleccion.name == "derecha")
                {
                    Debug.Log("Acierto");
                    record++;
                }
                else
                {
                    Debug.Log("Equivocado");
                    VerificarRecord();
                }

                GetRandomDireccion();
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (seleccion.name == "izquierda")
                {
                    Debug.Log("Acierto");
                    record++;
                }
                else
                {
                    Debug.Log("Equivocado");
                    VerificarRecord();
                }

                GetRandomDireccion();
            }

        }
        else
        {
            VerificarRecord();
        }
    }

    private void GetRandomDireccion()
    {
        if(seleccion != null)
        {
            seleccion.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
        }

        seleccion = direcciones[Random.Range(0,direcciones.Length)];
        seleccion.GetComponent<Renderer>().material.SetColor("_Color",Color.green);

        iniciarCuenta = Time.realtimeSinceStartup + tiempoRespuesta;
    }

    private void VerificarRecord()
    {
        GameOver.text = "GAME OVER";
        if(record > jugador.recordAtencion)
        {
            jugador.recordAtencion = record;
            PlayerPrefs.SetString("perfiles", JsonConvert.SerializeObject(listadoPerfiles));
            RecordTexto.text = jugador.recordAtencion.ToString();
        }

        sigueEnJuego = false;
    }
}
