using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perfil
{
    public int id;
    public string nombre_completo;
    public string apodo;
    public string correo_electronico;
    public int edad;
    public int recordAtencion;

    public Perfil(int id, string nom, string alias, string correo, string edad)
    {
        this.id = id;
        this.nombre_completo = nom;
        this.apodo = alias;
        this.correo_electronico = correo;
        this.edad = int.Parse(edad);
        this.recordAtencion = 0;
    }
    /*
    public Perfil(int id)
    {
        this.id = id;
        this.nombre_completo = "Bartolome Albarado Calles";
        this.apodo = "ElBartolo";
        this.correo_electronico = "no-reply@bart.com";
        this.edad = 10000;
    }*/ 
}
