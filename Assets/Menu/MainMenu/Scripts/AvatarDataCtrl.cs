using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AvatarDataCtrl : MonoBehaviour
{
    public Perfil data;
    public GameObject EditarPerfilV;
    // Start is called before the first frame update
    void Awake()
    {
        //EditarPerfilV = GameObject.Find("ActualizarPerfil");
    }

    public void AbrirVentanaEditar()
    {
        PerfilesCtrl.idPerfilSeleccionado = data.id;
        EditarPerfilV.SetActive(true);
        EditarPerfilV.GetComponent<ActualizarCtrl>().SetPerfil();
    }

    public void AbrirJuego()
    {
        PlayerPrefs.SetInt("idPerfilSeleccionado",data.id);
        SceneManager.LoadScene(1);
    }

    
}
